<?php
/**
 * @category Scandiweb
 * @author Edgars Suveizda <edgars.suveizda@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
declare(strict_types=1);

namespace Scandiweb\ItpAvoid\Controller\Cookie;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;

/**
 * Class SetCookie
 * @package Scandiweb\ItpAvoid\Controller\Cookie
 */
class SetCookie extends Action
{

    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $cookieMetaDataFactory;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * SetCookie constructor.
     * @param Context $context
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        JsonFactory $jsonFactory
    ) {
        $this->cookieManager = $cookieManager;
        $this->cookieMetaDataFactory = $cookieMetadataFactory;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function execute()
    {
        $result = $this->jsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $name = $this->getRequest()->getParam('name');
            $value = $this->getRequest()->getParam('value');
            $expires = $this->getRequest()->getParam('expires');

            if ($expires != '') {
                $expiresTimestamp = strtotime($expires);
                $todayTimeStamp = time();
                $duration = $expiresTimestamp - $todayTimeStamp;
                $metadata = $this->cookieMetaDataFactory->createPublicCookieMetadata()
                    ->setDuration($duration)
                    ->setPath('/');
            } else {
                $duration = null;
                $metadata = $this->cookieMetaDataFactory->createPublicCookieMetadata()
                    ->setPath('/');
            }
            if ($value === '') {
                $value = '0';
            }
            $test = [
                'name' => $name,
                'value' => $value,
                'expires' => $duration,
            ];
            $this->cookieManager->setPublicCookie($name, $value, $metadata);
            return $result->setData($test);
        }
        return $result;
    }
}
