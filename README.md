# ITP Avoid module

This  module is created for Safari ITP problem solution
Compatible with Magento 2 and higher versions
Module will intercept cookie that came from origin domain

## Instalation

install module core if not installed yet via `composer require scandiweb\module-core`

For now just create in `app/code`  directory  new directory `Scandiweb` and in that directory create another one 
named `ItpAvoid`. In `ItpAvoid` paste this code and run `php bin/magento s:up`

Later will be added installation  via composer

## Configuration

  - login in your admin panel
  - Go to Store/Configuration
  - Under Scandiweb Menu  search for ITP Avoid
  - make sure that module is enabled
  - enter list of cookies that you want intercept from ITP in proper field ans separate them with `,`
  - enter your root domain in specified field
  - save configuration
  - refresh your Store page and check does needed cookies is set as you expect