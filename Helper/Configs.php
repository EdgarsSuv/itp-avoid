<?php
/**
 * @category Scandiweb
 * @author Edgars Suveizda <edgars.suveizda@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

declare(strict_types=1);

namespace Scandiweb\ItpAvoid\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Configs
 * @package Scandiweb\ItpAvoid
 */
class Configs
{

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;


    const STORE_SCOPE = ScopeInterface::SCOPE_STORE;
    const XML_PATH_COOKIESLIST = 'scandiweb_itpavoid/general/cookiesList';
    const XML_PATH_MODULE_IS_ENABLED = 'scandiweb_itpavoid/general/enable';
    const XML_PATH_DOMAINNAME = 'scandiweb_itpavoid/general/domainName';

    /**
     * Configs constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_MODULE_IS_ENABLED, self::STORE_SCOPE);
    }

    /**
     * @return mixed
     */
    public function getCookies()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_COOKIESLIST, self::STORE_SCOPE);
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_DOMAINNAME, self::STORE_SCOPE);
    }
}
