/**
 * @category Scandiweb
 * @author Edgars Suveizda <edgars.suveizda@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

var config = {
    map: {
        '*': {
            cookieInterceptorModule: 'Scandiweb_ItpAvoid/js/cookie-interceptor'
        }
    }
};