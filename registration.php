<?php
/**
 * @category Scandiweb
 * @author Edgars Suveizda <edgars.suveizda@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
declare(strict_types=1);
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandiweb_ItpAvoid',
    __DIR__
);
