<?php
/**
 * @category Scandiweb
 * @author Edgars Suveizda <edgars.suveizda@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
declare(strict_types=1);

namespace Scandiweb\ItpAvoid\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Scandiweb\ItpAvoid\Helper\Configs;

/**
 * Class ItpAvoid
 * @package Scandiweb\ItpAvoid\Block
 */
class ItpAvoid extends Template
{

    /**
     * @var Configs
     */
    protected $configurations;

    /**
     * ItpAvoid constructor.
     * @param Context $context
     * @param Configs $configs
     */
    public function __construct(
        Context $context,
        Configs $configs
    ) {
        $this->configurations = $configs;
        parent::__construct($context);
    }

    /**
     * @return Configs
     */
    public function getConfigs()
    {
        return $this->configurations;
    }

    /**
     * @return string
     */
    public function getAjaxUrl()
    {
        $baseUrl = $this->getBaseUrl();
        $basePath = 'itpavoid/cookie/SetCookie';
        return $baseUrl . $basePath;
    }
}
